<?php

function find_even_index(array $arr) : int
{
    // Start with the first number 
    $left = $arr[0];
    // Start with the all the remaining values minus the first 
    $right = array_sum($arr) - $left;
    // If the remaining values match 0, then the match is the first item
    if ($right === 0)    {
        return 0;
    }
    // Go from the second value
    foreach (array_slice($arr, 1) as $key => $value) {
        // Take the new value to be considered away from the remaining sum
        $right -= $value;
        // Check for a match
        if ($left == $right) {
            return $key + 1;
        }
        // Add the value to the new left sum and continue.
        $left += $value;
    }

    return -1;
}